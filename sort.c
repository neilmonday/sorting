//Sorting algorithms in C
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 1000

void bubble_sort(int array[], int size)
{
    int i, j;
    for(i=0; i<size-1; i++){
        for(j=0; j<size-i-1; j++){
            if(array[j] > array[j+1]){
                int temp = array[j];
                array[j] = array[j+1];
                array[j+1] = temp;
            }
        }
    }
}

void insertion_sort(int array[], int size)
{
	int i, j, temp;
	for(i=0; i<size-1; i++) {
		temp = array[i+1];
		j = i;
		while((array[j]>temp) && (j >= 0))
		{
			array[j+1] = array[j];
			j--;
		}
		array[j+1] = temp;
	}
}

void merge(int array[], int first, int last, int middle)
{
    int helper_i = 0;
    int helper_array[last - first];             //A helper array to store temp merged array

    int i = first;                              //i is for iterating from 'first' through 'middle'
    int j = middle + 1;                         //j is for iterating from 'middle'+1 through 'last'

    while(( i <= middle) && (j <= last))        //when 'i' and 'j' are beyond 'middle' and 'last' respectully, we are done
    {
        if(array[i] <= array[j])
        {
            helper_array[helper_i] = array[i];
            helper_i++;
            i++;
        }
        else
        {
            helper_array[helper_i] = array[j];
            helper_i++;
            j++;
        }
    }
    while(i <= middle)                          //this means only the 'i' side has stuff left
    {
        helper_array[helper_i] = array[i];
        helper_i++;
        i++;
    }
    while(j <= last)                            //this means only the 'j' side has stuff left
    {
        helper_array[helper_i] = array[j];
        helper_i++;
        j++;
    }
    int k;
    for( k = 0; k < (last - first + 1); k++)    //copy the helper array back into the main array.
    {
        array[first + k] = helper_array[k];
    }
}

void merge_sort_r(int array[], int first, int last)
{
    if(first < last)
    {
        int middle = (first + last) / 2;      //pick a new middle point
        merge_sort_r(array, first, middle);   //split and sort the left side
        merge_sort_r(array, middle + 1, last);//split and sort the rigt side
        merge(array, first, last, middle);    //merge the 2 splitted and sorted sides
    }
}

void merge_sort(int array[], int size)
{
    merge_sort_r(array, 0, size-1);
}

int partition(int array[], int left, int right, int pivot_index)
{
    int temp = 0;
    //move the pivot to the end. instead of having to figure out
    //where in the array the pivot should go, just move it to the
    //end, and arrange array values according to that pivot, and
    //then move the pivot back into position after you are done.
    int pivot_value = array[pivot_index];       //move pivot to end
    array[pivot_index] = array[right];
    array[right] = pivot_value;
    int store_index = left;                     //start i and store_index at the leftmost item
    int i;
    for(i=left; i<right; i++)
    {

        if(array[i] <= pivot_value)
        {
            temp = array[i];
            array[i] = array[store_index];
            array[store_index] = temp;
            store_index++;
        }
    }
    temp = array[store_index];
    array[store_index] = array[right];
    array[right] = temp;
    return store_index;
}

void quick_sort_r(int array[], int left, int right)
{
    if(left < right)
    {
        int pivot_index = (int)((right-left)/2)+left;
        int pivot_new_index = partition(array, left, right, pivot_index);
        quick_sort_r(array, left, pivot_new_index - 1);
        quick_sort_r(array, pivot_new_index + 1, right);
    }
}

void quick_sort(int array[], int size)
{
    quick_sort_r(array, 0, size-1);
}

void populate(int array[], int size)
{
    int i;
    for(i = 0; i < size; i++)
    {
        array[i] = i;
    }
}

void shuffle(int array[], int size)
{
    if (size > 1)
    {
        size_t i;
        for (i = 0; i < size - 1; i++)
        {
            size_t j = i + rand() / (RAND_MAX / (size - i) + 1);
            int t = array[j];
            array[j] = array[i];
            array[i] = t;
        }
    }
}

void print_array(int array[], int size)
{
    int i;
    for(i=0; i<size; i++)
    {
        printf("%d, ", array[i]);
    }
    printf("\n");
}

void validate(int array[], int size)
{
    int i;
    int errors = 0;
    for(i=0; i<size; i++)
    {
        if(array[i] != i)
        {
            printf("Error! excpected %d, actual value: %d\n", i, array[i]);
            errors++;
        }
    }
    printf("Total errors: %d\n", errors);
}

int main()
{
    //seed random number generator with time.
    srand ( time(NULL) );

    int array[SIZE];

    populate(array, SIZE);
    shuffle(array, SIZE);

    print_array(array, SIZE);

    //sort
    //bubble_sort(array, SIZE);
    clock_t start, end;
    start = clock();
    //insertion_sort(array, SIZE);
    //merge_sort(array, SIZE);
    quick_sort(array, SIZE);
    end = clock();
    printf("\n");
    print_array(array, SIZE);
    printf("Time: %lldf\n", (long long int) end - start);
    validate(array, SIZE);
}
