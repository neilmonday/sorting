import random, time

#O(n) - O(n^2)
def bubble_sort(list):
    '''we cannot compare the last integer alone, we
    must compare it to it's partner, so we go from
    0 - len(list) - 1'''
    for i in range(0, len(list) - 1):                       #c if swapped is never True; otherwise n
        swapped = False
        '''you only need to compare len - i - 1 since the
        list is built from the back to the front'''
        for j in range(0, len(list) - i - 1):               #n/2 which = n since constants are reduced to 1
            if list[j] > list[j+1]:
                list[j], list[j+1] = list[j+1], list[j] #swap
                swapped = True
        if swapped == False:
            break
    return list


#O(n) - O(n^2)
def insertion_sort(list):
    for i in range(0, len(list) - 1):       #n
        temp = list[i+1]                    #c
        j = i                               #c
        while j >= 0 and list[j] > temp:    #anywhere from c to n
            list[j+1] = list[j]             #c
            j -= 1                          #c
        list[j+1] = temp                    #c
    return list                             #c

#O(n^2)
def selection_sort(list):
    for i in range(0, len(list)-1):             #n
        min = i
        for j in range(i, len(list) - 1):       #n/2
            if list[j+1] < list[min]:
                min = j+1
        list[i], list[min] = list[min], list[i]
    return list

#O(n * log(n))
def quick_sort(list):
    if(len(list) <= 1):
        return list
    pivot = int(len(list)/2)
    less = []
    greater = []
    for i in list:                          #n
        if i < list[pivot]:
            less.append(i)
        elif i == list[pivot]:
            pivot_array = []
            pivot_array.append(list[pivot])
        else:
            greater.append(i)
    return quick_sort(less) + pivot_array + quick_sort(greater) #2log(n) (2 goes away)

def heap_sort(list):
    first = 0
    last = len(list) - 1
    create_heap(list, first, last)                  #fully heapify
    for i in range(last, first, -1):                #loop through n times
        list[i], list[first] = list[first], list[i] #swap
        establish_heap_property (list, first, i - 1)#re-heapify
    return list

def create_heap(list, first, last):
    i = last/2                                      #only loop through entries that have children
    while i >= first:                               #re-heapify for every node that has children
        establish_heap_property(list, i, last)
        i -= 1

def establish_heap_property(list, first, last):
    while 2 * first + 1 <= last:                    #while the node has atleast 1 child
        k = 2 * first + 1                           #set k to be that 1st child
        if k < last and list[k] < list[k + 1]:      #if the node has 2 children, and the second child is greater than the first,
            k += 1                                  #then use the 2nd child
        if list[first] >= list[k]:                  #if the greater of the 2 children is less than or equal to their parent
            break                                   #then break since it is already in order
        list[first], list[k] = list[k], list[first] #swap the biggest child with the parent
        first = k                                   #move the parent down to the child we just swapped and compare it to its children

#array type
#list = random.sample(range(100), 100)  #random array
#list = range(100)[::-1]                #backwards array
list = range(0, 100)                    #

print list
start = time.clock()

#sorting algorithm
#list = bubble_sort(list)
#list = insertion_sort(list)
#list = selection_sort(list)
#list = quick_sort(list)
list = heap_sort(list)

end = time.clock()
print "%.2gs" % (end-start)
print list
